#!/bin/bash

EXE_LOC="/usr/bin"

TCP_CONNECTED=$(${EXE_LOC}/ss -tn state connected | wc -l)
UDP_LISTENING=$(${EXE_LOC}/ss -uln | wc -l)
TOTAL_PORTS=$(${EXE_LOC}/sysctl net.ipv4.ip_local_port_range | ${EXE_LOC}/awk '{print $4 - $3}')
AVAILABLE_UDP=$(${EXE_LOC}/expr ${TOTAL_PORTS} - ${UDP_LISTENING})
AVAILABLE_TCP=$(${EXE_LOC}/expr ${TOTAL_PORTS} - ${TCP_CONNECTED})

${EXE_LOC}/logger -s "TCP_CONNECTED=${TCP_CONNECTED} UDP_LISTENING=${UDP_LISTENING} TOTAL_PORTS=${TOTAL_PORTS} AVAILABLE_TCP=${AVAILABLE_TCP} AVAILABLE_UDP=${AVAILABLE_UDP}"

exit 0