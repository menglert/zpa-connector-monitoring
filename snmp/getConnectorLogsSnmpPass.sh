#!/bin/sh -f

EXE_LOC="/usr/bin"
ZSCALER_OID=".1.3.6.1.4.1.46262"
ZPA_OID="${ZSCALER_OID}.10"
REQUESTED_OID="$2"

if [ "$1" = "-n" ]; then
  case "${REQUESTED_OID}" in
    ${ZPA_OID}.1 | \
    ${ZPA_OID}.1.0 | \
    ${ZPA_OID}.1.1 | \
    ${ZPA_OID}.0 | \
    ${ZPA_OID}.0.* | \
    ${ZPA_OID}) REQUESTED_OID=${ZPA_OID}.1.1.0;;
    ${ZPA_OID}.1.1.0) REQUESTED_OID=${ZPA_OID}.1.2.0;;
    ${ZPA_OID}.1.2.0) REQUESTED_OID=${ZPA_OID}.1.3.0;;
    ${ZPA_OID}.1.3.0) REQUESTED_OID=${ZPA_OID}.1.4.0;;
    ${ZPA_OID}.1.4.0) REQUESTED_OID=${ZPA_OID}.1.5.0;;
    ${ZPA_OID}.1.5.0) REQUESTED_OID=${ZPA_OID}.1.6.0;;
    ${ZPA_OID}.2 | \
    ${ZPA_OID}.2.0 | \
    ${ZPA_OID}.2.1 | \
    ${ZPA_OID}.2.1.1 | \
    ${ZPA_OID}.2.1.0 | \
    ${ZPA_OID}.1.6.0) REQUESTED_OID=${ZPA_OID}.2.1.1.0;;
    ${ZPA_OID}.2.1.1.0) REQUESTED_OID=${ZPA_OID}.2.1.2.0;;
    ${ZPA_OID}.2.1.2.0) REQUESTED_OID=${ZPA_OID}.2.1.3.0;;
    ${ZPA_OID}.2.1.3.0) REQUESTED_OID=${ZPA_OID}.2.2.1.0;;
    ${ZPA_OID}.2.2 | \
    ${ZPA_OID}.2.2.1 | \
    ${ZPA_OID}.2.2.0 | \
    ${ZPA_OID}.2.2.1.0) REQUESTED_OID=${ZPA_OID}.2.2.2.0;;
    ${ZPA_OID}.2.2.2.0) REQUESTED_OID=${ZPA_OID}.3.1.1.0;;
    ${ZPA_OID}.3 | \
    ${ZPA_OID}.3.0 | \
    ${ZPA_OID}.3.1 | \
    ${ZPA_OID}.3.1.1 | \
    ${ZPA_OID}.3.1.0 | \
    ${ZPA_OID}.3.1.1.0) REQUESTED_OID=${ZPA_OID}.3.1.2.0;;
    ${ZPA_OID}.3.1.2.0) REQUESTED_OID=${ZPA_OID}.3.1.3.0;;
    ${ZPA_OID}.3.1.3.0) REQUESTED_OID=${ZPA_OID}.3.1.4.0;;
    ${ZPA_OID}.3.1.4.0) REQUESTED_OID=${ZPA_OID}.3.1.5.0;;
    ${ZPA_OID}.3.1.5.0) REQUESTED_OID=${ZPA_OID}.3.1.6.0;;
    ${ZPA_OID}.3.1.6.0) REQUESTED_OID=${ZPA_OID}.3.1.7.0;;
    ${ZPA_OID}.3.1.7.0) REQUESTED_OID=${ZPA_OID}.3.1.8.0;;
    ${ZPA_OID}.3.1.8.0) REQUESTED_OID=${ZPA_OID}.3.1.9.0;;
    ${ZPA_OID}.3.1.9.0) REQUESTED_OID=${ZPA_OID}.3.1.10.0;;
    ${ZPA_OID}.3.1.10.0) REQUESTED_OID=${ZPA_OID}.3.1.11.0;;
    ${ZPA_OID}.3.1.11.0) REQUESTED_OID=${ZPA_OID}.3.2.1.0;;
    ${ZPA_OID}.3.2 | \
    ${ZPA_OID}.3.2.1 | \
    ${ZPA_OID}.3.2.0 | \
    ${ZPA_OID}.3.2.1.0) REQUESTED_OID=${ZPA_OID}.3.2.2.0;;
    ${ZPA_OID}.3.2.2.0) REQUESTED_OID=${ZPA_OID}.3.2.3.0;;
    ${ZPA_OID}.3.2.3.0) REQUESTED_OID=${ZPA_OID}.3.2.4.0;;
    ${ZPA_OID}.3.2.4.0) REQUESTED_OID=${ZPA_OID}.3.2.5.0;;
    ${ZPA_OID}.3.2.5.0) REQUESTED_OID=${ZPA_OID}.3.2.6.0;;
    ${ZPA_OID}.3.2.6.0) REQUESTED_OID=${ZPA_OID}.3.2.7.0;;
    ${ZPA_OID}.3.2.7.0) REQUESTED_OID=${ZPA_OID}.3.2.8.0;;
    ${ZPA_OID}.3.2.8.0) REQUESTED_OID=${ZPA_OID}.3.2.9.0;;
    ${ZPA_OID}.3.2.9.0) REQUESTED_OID=${ZPA_OID}.4.1.0;;
    ${ZPA_OID}.4 | \
    ${ZPA_OID}.4.0 | \
    ${ZPA_OID}.4.1 | \
    ${ZPA_OID}.4.1.0) REQUESTED_OID=${ZPA_OID}.4.2.0;;
    ${ZPA_OID}.4.2.0) REQUESTED_OID=${ZPA_OID}.4.3.0;;
    ${ZPA_OID}.4.3.0) REQUESTED_OID=${ZPA_OID}.4.4.0;;
    ${ZPA_OID}.4.4.0) REQUESTED_OID=${ZPA_OID}.4.5.0;;
    ${ZPA_OID}.4.5.0) REQUESTED_OID=${ZPA_OID}.4.6.0;;
    ${ZPA_OID}.4.6.0) REQUESTED_OID=${ZPA_OID}.5.1.0;;
    ${ZPA_OID}.5 | \
    ${ZPA_OID}.5.0 | \
    ${ZPA_OID}.5.1 | \
    ${ZPA_OID}.5.1.0) REQUESTED_OID=${ZPA_OID}.5.2.0;;
    ${ZPA_OID}.5.2.0) REQUESTED_OID=${ZPA_OID}.5.3.0;;
    ${ZPA_OID}.5.3.0) REQUESTED_OID=${ZPA_OID}.5.4.0;;
    ${ZPA_OID}.5.4.0) REQUESTED_OID=${ZPA_OID}.5.5.0;;
    ${ZPA_OID}.5.5.0) REQUESTED_OID=${ZPA_OID}.5.6.0;;
    ${ZPA_OID}.5.6.0) REQUESTED_OID=${ZPA_OID}.5.7.0;;
    ${ZPA_OID}.5.7.0) REQUESTED_OID=${ZPA_OID}.5.8.0;;
    ${ZPA_OID}.5.8.0) REQUESTED_OID=${ZPA_OID}.5.9.0;;
    *) exit 0 ;;
  esac
fi

case "${REQUESTED_OID}" in
    ${ZPA_OID}.1.1.0) FILTER="Mtunnels"; METRIC="total";;
    ${ZPA_OID}.1.2.0) FILTER="Mtunnels"; METRIC="to broker";;
    ${ZPA_OID}.1.3.0) FILTER="Mtunnels"; METRIC="to private broker";;
    ${ZPA_OID}.1.4.0) FILTER="Mtunnels"; METRIC="unbound/errored";;
    ${ZPA_OID}.1.5.0) FILTER="Mtunnels"; METRIC="current active";;
    ${ZPA_OID}.1.6.0) FILTER="Mtunnels"; METRIC="peak active";;
    ${ZPA_OID}.2.1.1.0) FILTER="Broker data connection"; METRIC="total";;
    ${ZPA_OID}.2.1.2.0) FILTER="Broker data connection"; METRIC="active";;
    ${ZPA_OID}.2.1.3.0) FILTER="Broker data connection"; METRIC="backed off connections";;
    ${ZPA_OID}.2.2.1.0) FILTER="Broker data transfer"; METRIC="to broker";;
    ${ZPA_OID}.2.2.2.0) FILTER="Broker data transfer"; METRIC="from broker";;
    ${ZPA_OID}.3.1.1.0) FILTER="RPC Messages"; METRIC="RX:.*BrkRq.*Dsp";;
    ${ZPA_OID}.3.1.2.0) FILTER="RPC Messages"; METRIC="RX:.*BrkRq.*UBrk";;
    ${ZPA_OID}.3.1.3.0) FILTER="RPC Messages"; METRIC="RX:.*BrkRq.*PBrk";;
    ${ZPA_OID}.3.1.4.0) FILTER="RPC Messages"; METRIC="RX:.*BindAck";;
    ${ZPA_OID}.3.1.5.0) FILTER="RPC Messages"; METRIC="RX:.*AppRtDisc";;
    ${ZPA_OID}.3.1.6.0) FILTER="RPC Messages"; METRIC="RX:.*DnsChk.*Dsp";;
    ${ZPA_OID}.3.1.7.0) FILTER="RPC Messages"; METRIC="RX:.*DnsChk.*Pbrk";;
    ${ZPA_OID}.3.1.8.0) FILTER="RPC Messages"; METRIC="RX:.*MtunnelEnd";;
    ${ZPA_OID}.3.1.9.0) FILTER="RPC Messages"; METRIC="RX:.*TagPause";;
    ${ZPA_OID}.3.1.10.0) FILTER="RPC Messages"; METRIC="RX:.*TagResume";;
    ${ZPA_OID}.3.1.11.0) FILTER="RPC Messages"; METRIC="RX:.*WinUpdate";;
    ${ZPA_OID}.3.2.1.0) FILTER="RPC Messages"; METRIC="TX|TX_DROP:.*BrkRqAck.*Dsp";;
    ${ZPA_OID}.3.2.2.0) FILTER="RPC Messages"; METRIC="TX|TX_DROP:.*BrkRqAck.*UBrk";;
    ${ZPA_OID}.3.2.3.0) FILTER="RPC Messages"; METRIC="TX|TX_DROP:.*BrkRqAck.*PBrk";;
    ${ZPA_OID}.3.2.4.0) FILTER="RPC Messages"; METRIC="TX|TX_DROP:.*BindReq";;
    ${ZPA_OID}.3.2.5.0) FILTER="RPC Messages"; METRIC="TX|TX_DROP:.*AppRtReq";;
    ${ZPA_OID}.3.2.6.0) FILTER="RPC Messages"; METRIC="TX|TX_DROP:.*HealthRp";;
    ${ZPA_OID}.3.2.7.0) FILTER="RPC Messages"; METRIC="TX|TX_DROP:.*DnsChk.*Dsp";;
    ${ZPA_OID}.3.2.8.0) FILTER="RPC Messages"; METRIC="TX|TX_DROP:.*DnsChk.*PBrk";;
    ${ZPA_OID}.3.2.9.0) FILTER="RPC Messages"; METRIC="TX|TX_DROP:.*DnsChk.*NoR";;
    ${ZPA_OID}.4.1.0) FILTER="Registered apps"; METRIC="count";;
    ${ZPA_OID}.4.2.0) FILTER="Registered apps"; METRIC="alive apps";;
    ${ZPA_OID}.4.3.0) FILTER="Registered apps"; METRIC="apps with on-access health";;
    ${ZPA_OID}.4.4.0) FILTER="Registered apps"; METRIC="service count";;
    ${ZPA_OID}.4.5.0) FILTER="Registered apps"; METRIC="target count";;
    ${ZPA_OID}.4.6.0) FILTER="Registered apps"; METRIC="target alive";;
    ${ZPA_OID}.5.1.0) FILTER="System Sockets:"; METRIC="Created";;
    ${ZPA_OID}.5.2.0) FILTER="System Sockets:"; METRIC="TCP4 in-use";;
    ${ZPA_OID}.5.3.0) FILTER="System Sockets:"; METRIC="TCP4 time-wait";;
    ${ZPA_OID}.5.4.0) FILTER="System Sockets:"; METRIC="UDP4 in-use";;
    ${ZPA_OID}.5.5.0) FILTER="System Sockets:"; METRIC="TCP6 in-use";;
    ${ZPA_OID}.5.6.0) FILTER="System Sockets:"; METRIC="UDP6 in-use";;
    ${ZPA_OID}.5.7.0) FILTER="System Sockets:"; METRIC="Ports available per interface";;
    ${ZPA_OID}.5.8.0) FILTER="System Sockets:"; METRIC="IPv4 interfaces";;
    ${ZPA_OID}.5.9.0) FILTER="System Sockets:"; METRIC="IPv6 interfaces";;
    *) exit 0 ;;
esac

RAW_LINE=$(${EXE_LOC}/journalctl -u zpa-connector \
    --since "1 minute ago" \
    --no-pager \
    -o cat \
    | ${EXE_LOC}/grep "${FILTER}" \
    | ${EXE_LOC}/tail -1)

${EXE_LOC}/echo "${REQUESTED_OID}"
${EXE_LOC}/echo "integer"
${EXE_LOC}/echo "${RAW_LINE}" | ${EXE_LOC}/sed -e "s@.*${METRIC} \([0-9]*\).*@\1@"
    
exit 0