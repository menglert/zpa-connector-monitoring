#!/bin/sh -f

EXE_LOC="/usr/bin"
ZSCALER_OID=".1.3.6.1.4.1.46262"
ZPA_OID="${ZSCALER_OID}.10"
REQUESTED_OID="$2"

DATE_START="$(${EXE_LOC}/systemctl show zpa-connector --property=ActiveEnterTimestamp | ${EXE_LOC}/sed -e 's/.*=\(.*\)/\1/')"
EPOCH_START=$(${EXE_LOC}/date --date "${DATE_START}" +%s)
EPOCH_CURRENT=$(${EXE_LOC}/date +%s)

if [ "$1" = "-n" ]; then
  case "${REQUESTED_OID}" in
    ${ZPA_OID}.6 | \
    ${ZPA_OID}) REQUESTED_OID=${ZPA_OID}.6.0;;
    *) exit 0 ;;
  esac
fi

case "${REQUESTED_OID}" in
    ${ZPA_OID}.6.0) UPTIME=$(${EXE_LOC}/expr ${EPOCH_CURRENT} - ${EPOCH_START});;
    *) exit 1 ;;
esac

${EXE_LOC}/echo "${REQUESTED_OID}"
${EXE_LOC}/echo "timeticks"
${EXE_LOC}/echo "${UPTIME}"

exit 0